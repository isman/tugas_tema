import 'package:flutter/material.dart';

Color purpleColor = Color(0xff4a148c);
Color whiteColor = Color(0xffFFFFFF);
Color lightPurpleColor = Color(0xffffb2ff);
Color blackColor = Color(0xff2C3A59);
Color greyColor = Color(0xff808BA2);
Color greenColor = Color(0xff29CB9E);

TextStyle titleTextStyle = TextStyle(
  color: blackColor,
  fontWeight: FontWeight.w500,
  fontSize: 16,
);

TextStyle subtitleTextStyle = TextStyle(
  color: greyColor,
  fontWeight: FontWeight.w300,
);
