# Tugas Tema Flutter

Tugas Mata Kuliah Mobile Programming Flutter Theme
[Link Aplikasi](tugas_tema.apk)

# Kelas

TIF RM 18 CNS

# Anggota

- [Ade Isman Aji            - 18 111 179](https://gitlab.com/isman)
- [Moch Dzulvie Aldyansyah  - 18 111 211](https://gitlab.com/mochdzulvie)
- [Sindu Prakasa Lesmana    - 18 111 297](https://gitlab.com/sinduprakasa04)
- [Zeni Malik Abdulah       - 18 111 237](https://gitlab.com/zenimalikabdulah)
